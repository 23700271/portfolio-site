// Import React
import React from 'react';

// Render page footer
function Footer() {
    return (
        <footer>
            <p>nathanwpope@gmail.com</p>
            <p>2020 Nathan Pope</p>
        </footer>
    );
}

// Export Footer component for App.js
export default Footer;
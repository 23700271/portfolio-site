// Import React
import React from 'react';
// Import React Router NavLink
import { NavLink } from 'react-router-dom';

// Render the page header with navbar
function Header() {
    return (
        <header>
            <nav>    
                <ul>
                    {/* Link to about page which loads the Home component */}
                    <li><NavLink exact to='/'>About</NavLink></li>
                    {/* Link to portfolio page which loads the Portfolio component */}
                    <li><NavLink to='/portfolio'>Portfolio</NavLink></li>
                    {/* Link to CV hosted on Google Drive */}
                    <li><a href='https://drive.google.com/open?id=1F9F2Fh3uoG9Zw-ZYT4tYBF3Y1wgMV3Q7'>CV</a></li>
                </ul>
            </nav>
        </header>
    );
}

// Export Header component for App.js
export default Header;
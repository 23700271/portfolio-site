// Import React
import React from 'react';

// Import React Router Link
import { Link } from 'react-router-dom';

// Import images
import BeeCentre from '../images/bee-centre/bee-centre-home.webp';
import BioStore from '../images/bio-store/bio-store-home.webp';
import PortfolioImg from '../images/portfolio-site/about.webp';

// Render the Portfolio Site section of the projects list
function ProjectPortfolioSite() {
    return (
        <section>
            <Link to='/portfolio/portfolio-site'>
                <h2>Portfolio Site</h2>
                <p>UI / UX Design & Front-End Development</p>
                <img src={PortfolioImg} alt="Nathan Pope's professional portfolio portfolio page." />             
            </Link>
        </section>
    );
}

// Render the Bio-Store section of the projects list
function ProjectBioStore() {
    return (
        <section className='portfolio-project'>
            <Link to='/portfolio/bio-store'>
                <h2>Bio-Store</h2>
                <p>UI / UX Design & Front-End Development</p>   
                <img src={BioStore} alt="Bio-Store home page for desktop." />                
            </Link>
        </section>
    );
}

// Render The Bee Centre section of the projects list
function ProjectBeeCentre() {
    return (
        <section>
            <Link to='/portfolio/the-bee-centre'>
                <h2>The Bee Centre</h2>
                <p>UI / UX Design & Front-End Development</p>    
                <img src={BeeCentre} alt="The Bee Centre home page for desktop." />          
            </Link>
        </section>
    );
}

// Renders the portfolio page
function Portfolio() {
    return (
        <main id='portfolio-page'>
            <h1>Portfolio</h1>
            <ul id='projects-list'>
                <li><ProjectBeeCentre /></li>
                <li><ProjectBioStore /></li>
                <li><ProjectPortfolioSite /></li>
            </ul>
        </main>
    );
}

// Export Portfolio component for App.js
export default Portfolio;
// Import React
import React from 'react';

// Import images for the Bio-Store project page.
import Home from '../../images/bio-store/bio-store-home.webp';
import Team from '../../images/bio-store/bio-store-team.webp';
import Figma_Mobile from '../../images/bio-store/bio-store-figma-mobile.webp';
import Figma_Desktop from '../../images/bio-store/bio-store-figma-desktop.webp';
import Products from '../../images/bio-store/bio-store-products.webp';

// Renders the Bio-Store page
function BioStore() {
    return (
        <main className='project-page'>
            {/* Project title and services */}
            <div className='page-title'>
                <h1>Bio-Store</h1>
                <p>UI / UX Design & Front-End Development</p>
                {/* Link to Bio-Store website */}
                <a href="https://bio-store-23700271.netlify.app/">Website</a>
                {/* Link to Bio-Store repository */}
                <a href="https://bitbucket.org/23700271/bio-store/src/master/">Repository</a>
            </div>

            {/* Screenshot of the Bio-Store home page. */}
            <div className='project-img-container'>
                <img src={Home} alt="A screenshot of the Bio-Store website home page." />
            </div>

            <p>The Bio-Store are a company in North West England who sell biodegradable products 
                to businesses around the world. They needed a website to advertise their products 
                and services, and encourage businesses to contact them.</p>

            {/* Screenshot of the Bio-Store team page. */}
            <div className='project-img-container'>
                <img src={Team} alt="A screenshot of the Bio-Store website home page." />
            </div>
            
            <p>This project was a full design and development build starting with a website design 
                brief provided by the company. Initial steps focused on identifying requirements and
                planning the layout.</p>

            {/* Screenshot of Figma Bio-Store prototypes for mobile. */}
            <div className='project-img-container'>
                <img src={Figma_Mobile} alt="A screenshot of the Figma high-fidelity prototypes for mobile." />
            </div>

            <p>Continuing from the initial design phase, high-fidelity prototypes were designed for 
                mobile and desktop devices using Figma.</p>

            {/* Screenshot of Figma Bio-Store prototypes for desktop. */}
            <div className='project-img-container'>
                <img src={Figma_Desktop} alt="A screenshot of the Figma high-fidelity prototypes for desktop." />
            </div>

            <p>Once the website was fully planned out it was developed with React, starting with 
                functionality and then design.</p>
            
            {/* Screenshot of the Bio-Store website team page. */}
            <div className='project-img-container'>
                <img src={Products} alt="A screenshot of the Bio-Store website products page." />
            </div>

        </main>
    );
}

// Export BioStore component for App.js
export default BioStore;
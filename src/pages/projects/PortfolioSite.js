// Import React
import React from 'react';

// Import images for the Professional Portfolio project page
import About from '../../images/portfolio-site/about.webp';
import Planning from '../../images/portfolio-site/planning.webp';
import Portfolio from '../../images/portfolio-site/portfolio.webp';

// Renders the Professional Portfolio project page
function PortfolioSite() {
    return (
        <main>
            {/* Project title and services */}
            <div className='page-title'>
                <h1>Portfolio Site</h1>
                <p>UI / UX Design & Front-End Development</p>
                {/* Professional Portfolio repository */}
                <a href="https://bitbucket.org/23700271/portfolio-site/src/master/">Repository</a>
            </div>

            {/* Screenshot of my Professional Portfolio about page. */}
            <div className='project-img-container'>
                <img src={About} alt="A screenshot of the Professional Portfolio about page." />
            </div>

            <p>My professional portfolio site was created to showcase my previous work and to allow
                potential employers to learn more about me. The site includes links to my most recent
                projects, my CV and an about page with my primary skills listed.</p>

            {/* Photograph of portfolio site planning. */}
            <div className='project-img-container'>
                <img src={Planning} alt="A photograph of some early notes and sketches for the portfolio website." />
            </div>

            <p>The design was planned with pen and paper sketches and notes. The intent was to
                create a site that was clean, simple and professional providing only essential
                information.</p>

            {/* Screenshot of my Professional Portfolio about page. */}
            <div className='project-img-container'>
                <img src={Portfolio} alt="A screenshot of the Professional Portfolio portfolio page." />
            </div>
            <p></p>
        </main>
    );
}

// Exports PortfolioSite component for App.js
/**
 * Component wrapped in withRouter to correctly render component when 
 * returning to page with browser back button.
 */
export default PortfolioSite;
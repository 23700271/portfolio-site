// Import React
import React from 'react';

// Import images for The Bee Centre project page.
import Home from '../../images/bee-centre/bee-centre-home.webp';
import spider_diagram from '../../images/bee-centre/spider-diagram.webp';
import sketches from '../../images/bee-centre/sketches.webp';
import figma from '../../images/bee-centre/bee-centre-figma.webp';
import Team from '../../images/bee-centre/bee-centre-team.webp';

// Renders The Bee Centre project page.
function BeeCentre() {
    return (
        <main className='project-page'>
            {/* Project title and services */}
            <div className='page-title'>
                <h1>The Bee Centre</h1>
                <p>UI / UX Design & Front-End Development</p>
                {/* Link to The Bee Centre website */}
                <a href="https://the-bee-centre-23700271.netlify.app/">Website</a>
                {/* Link to The Bee Centre repository */}
                <a href="https://bitbucket.org/23700271/the-bee-centre/src/master/">Repository</a>
            </div>

            {/* Screenshot of The Bee Centre home page. */}
            <div className='project-img-container'>
                <img src={Home} alt="A screenshot of The Bee Centre website home page." />
            </div>

            <p>The Bee Centre is a company based in North West England who wanted a website to 
            promote their business and encourage people to get in touch.</p>

            {/* Spider diagram photograph */}
            <div className='project-img-container'>
                <img 
                    src={spider_diagram} 
                    alt='A spider diagram for listing requirements from the website design brief for The Bee Centre.' 
                />
            </div>

            <p>The project started by studying a website design brief provided by the company to 
                determine the necessary website requirements. Once the details were worked out, 
                low-fidelity prototypes were sketched to plan the website structure.</p>
            {/* Picture of low-fidelity sketches */}
            <div className='project-img-container'>
                <img 
                    src={sketches} 
                    alt='A few low-fidelity sketches from planning the navbar of The Bee Centre.' 
                />
            </div>

            <p>Following on from the low-fidelity sketches, high-fidelity prototypes were then created 
                using Figma to design the website in much greater detail.</p>
            
            {/* Screenshot of Figma prototype */}
            <div className='project-img-container'>
                <img src={figma} alt='An early Figma prototype for The Bee Centre website.' />
            </div>

            <p>The website was then developed using HTML and Sass following the prototype designs.
                Additional changes were then made for improved usability and based on client feedback.</p>
            
            {/* Screenshot of The Team page from The Bee Centre website */}
            <div className='project-img-container'>
                <img src={Team} alt="A screenshot of The Team page from The Bee Centre website." />
            </div>
        </main>
    );
}

// Export BeeCentre component for App.js
export default BeeCentre;
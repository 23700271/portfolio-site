// Import React
import React from 'react';

// Renders the about page
function Home() {
    return (
        <main>
            {/* Page title and subtitles */}
            <div className='page-title'>
                <h1>Nathan Pope</h1>
                <p>UI / UX Design<br></br>Front-End Development<br></br>Back-End Developemnt</p>
            </div>
            {/* About me section */}
            <section id='home-about'>
                <h2>About</h2>
                <p>Hi, I'm Nathan, an award-winning final year Web Design and Development student at Edge Hill University.</p>
                <p>I recently completed my research & development project for a progressive web
                    app for recipes aimed at students and designed and developed three websites for my professional portfolio.</p>
                <p>I'm now looking to start my career working in either a front-end or back-end design and development role.</p>
            </section>
            {/* My skills section */}
            <section id='home-skills'>
                <h2>Skills</h2>
                <p>I have a range of skills for both front-end and back-end design and development.
                    Check out my CV, view my portfolio or contact me directly by email if you'd like to know more!</p>
                <ul>
                    <li>React</li>
                    <li>JavaScript</li>
                    <li>Sass</li>
                    <li>HTML5 / CSS3</li>
                    <li>UI / UX Design</li>
                    <li>PHP</li>
                    <li>Ember.js</li>
                    <li>Laravel</li>
                    <li>Python</li>
                    <li>Git</li>
                    <li>Mercurial</li>
                    <li>MySQL</li>
                    <li>Command Line</li>
                    <li>Java</li>
                    <li>Linux</li>
                    <li>Windows</li>
                </ul>
            </section>
        </main>
    );
}

// Export Home component for App.js
export default Home;
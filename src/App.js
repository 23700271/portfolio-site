// Import React
import React from 'react';
// Import React Router
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
// Import stylesheet
import './App.scss';
// Import app components
import ScrollToTop from './ScrollToTop';
import Header from './common/Header.js';
import Footer from './common/Footer.js';
import Home from './pages/Home.js';
import Portfolio from './pages/Portfolio.js';
import BeeCentre from './pages/projects/BeeCentre.js';
import BioStore from './pages/projects/BioStore.js';
import PortfolioSite from './pages/projects/PortfolioSite.js';

// Renders the app and handles route switching between URL paths
function App() {
  return (
    <Router>
    <ScrollToTop />
    <Header />
      <Switch>
        <Route path='/portfolio/portfolio-site'>
          <PortfolioSite />
        </Route>
        <Route path='/portfolio/bio-store'>
          <BioStore />
        </Route>
        <Route path='/portfolio/the-bee-centre'>
          <BeeCentre />
        </Route>
        <Route path='/portfolio'>
          <Portfolio />
        </Route>
        <Route path='/'>
          <Home />
        </Route>
      </Switch>
      <Footer />
    </Router>
  );
}

// Export App component for index.js
export default App;
